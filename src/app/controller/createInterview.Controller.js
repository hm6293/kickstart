(function() {
    angular.module('kickstart').controller('createInterviewCtrl', createInterviewCtrl);

    function createInterviewCtrl(appService, toastService, $scope, $state, $timeout) {
        var createInterview = this;
        createInterview.data = {};
        appService.getTrades().then(function(response) {
            createInterview.allDropdowns = response.data;
        });
        appService.getJobseekersAutocomplete({}).then(function(response) {
            createInterview.jobseekers = response.data;
        });
        appService.getVacanciesAutocomplete({}).then(function(response) {
            createInterview.vacancies = response.data;
        });
        createInterview.create = function(callbacks) {
            appService.createInterview(createInterview.data).then(function(result) {
                $timeout(function() {
                    $state.go('main.interviews');
                    toastService.show('Interview created');
                }, 1000);

            }).finally(function() {
                callbacks.formSubmitted();
            })
        }
    }
})();
