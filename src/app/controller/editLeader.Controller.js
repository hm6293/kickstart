(function() {
    angular.module('kickstart').controller('editLeaderCtrl', editLeaderCtrl);

    function editLeaderCtrl(appService, toastService, $scope, $state, $stateParams, $timeout) {
        var editLeader = this;
        var leader = {
            id: $stateParams.leaderId
        };
        var oldNumber = '';
        appService.getLeaders(leader).then(function(result) {
            editLeader.data = result.data[0];
            editLeader.data.lat = result.data[0].location.lat;
            editLeader.data.long = result.data[0].location.long;
            oldNumber = editLeader.data.mobile;

        });

        editLeader.edit = function(callbacks) {
            appService.editLeader(editLeader.data, leader.id, oldNumber).then(function(result) {
                $timeout(function() {
                    $state.go('main.leaders');
                    toastService.show('Leader edited');
                }, 1000);

            }).finally(function() {
                callbacks.formSubmitted();
            })
        }
        editLeader.goToLeaders = function() {
            $state.go('main.leaders');
        }
    }
})();
