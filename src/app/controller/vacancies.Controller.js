(function() {
    angular.module('kickstart').controller('vacanciesCtrl', vacanciesCtrl);

    function vacanciesCtrl(appService, toastService, $scope, $state, $mdEditDialog, $mdDialog) {
        var vacancies = this;
        vacancies.query = {
            limit: 5,
            page: 1
        };
        vacancies.getVacancies = function(page, limit) {
            vacancies.query.limit = limit;
            vacancies.query.page = page;
            appService.getVacancies(vacancies.query).then(function(response) {
                vacancies.vacancies = response;
            });
        }
        vacancies.getVacancies(vacancies.query.page, vacancies.query.limit);
        vacancies.showDetails = function(vacancy) {
            $mdDialog.show({
                locals: { vacancy: vacancy },
                clickOutsideToClose: true,
                templateUrl: 'app/views/vacancy.html',
                controller: function($scope, vacancy) {
                    $scope.vacancy = vacancy;
                }
            });
        }
        vacancies.goToCreateVacancy = function() {
            $state.go('main.createVacancy');
        }
        vacancies.goToEditVacancy = function(event, vacancyId) {
            event.stopPropagation();
            $state.go('main.editVacancy', { vacancyId: vacancyId });
        }
        vacancies.deleteVacancy = function(event, vacancyId, index) {
            event.stopPropagation();
            appService.deleteVacancy(vacancyId).then(function(response) {
                toastService.show('Vacancy deleted successfully');
                vacancies.getVacancies(vacancies.query.page, vacancies.query.limit);

            });
        }
    }
})();
