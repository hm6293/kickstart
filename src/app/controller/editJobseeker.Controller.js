(function() {
    angular.module('kickstart').controller('editJobseekerCtrl', editJobseekerCtrl);

    function editJobseekerCtrl(appService, toastService, $scope, $state, $stateParams, $timeout) {
        var editJobseeker = this;
        var oldNumber = '';
        var jobseeker = {
            id: $stateParams.jobseekerId
        };
        //get list of all leaders
        appService.getLeadersAutocomplete({}).then(function(response) {
            editJobseeker.leaders = response.data;
        });
        appService.getJobseekers(jobseeker).then(function(result) {
            editJobseeker.data = result.data[0];
            oldNumber = editJobseeker.data.mobile;
            editJobseeker.data.lat = result.data[0].location.lat;
            editJobseeker.data.long = result.data[0].location.long;
            if (editJobseeker.data.dateOfBirth)
                editJobseeker.data.dateOfBirth = new Date(parseInt(editJobseeker.data.dateOfBirth));
            editJobseeker.data.files = [];
            var trades = [];
            angular.forEach(editJobseeker.data.trades, function(value, key) {
                trades.push({ name: key, value: value });
            });
            editJobseeker.data.trades = trades;
        });
        editJobseeker.addTrade = function() {
            editJobseeker.data.trades.push({ name: '', value: '' });
        }
        editJobseeker.removeTrade = function(index) {
            if (editJobseeker.data.trades.length > 1)
                editJobseeker.data.trades.splice(index, 1);
        }
        appService.getTrades().then(function(response) {
            editJobseeker.allDropdowns = response.data;
        })

        editJobseeker.getFile = function(files, $errors) {
            if (angular.isDefined($errors) && ($errors.filesize.length || $errors.incorrectFile.length)) {
                var imageError = '';
                if ($errors.filesize.length) {
                    imageError += 'File size exceeds 5 mb for ' + $errors.filesize.join(', ');
                }
                if ($errors.incorrectFile.length) {
                    imageError += '\n Incorrect file type for ' + $errors.incorrectFile.join(', ');
                }
                toastService.showError(imageError);
            } else {
                $scope.app.setImage(files[0]).then(function(result) {
                    editJobseeker.data.file = files[0];
                })
            }
        }
        editJobseeker.edit = function(callbacks) {

            appService.editJobseeker(editJobseeker.data, jobseeker.id, oldNumber).then(function(result) {
                $timeout(function() {
                    $state.go('main.jobseekers');
                    toastService.show('Jobseeker created');
                }, 1000);

            }).finally(function() {
                callbacks.formSubmitted();
            })
        }
        editJobseeker.goToJobseekers = function() {
            $state.go('main.jobseekers');

        }
    }
})();
