(function() {
    angular.module('kickstart').controller('appCtrl', appCtrl);

    function appCtrl($rootScope, $state, $q) {
        var app = this;
        $rootScope.$on('userAuthenticated', function() {
            $state.transitionTo('main.dashboard');
        });
        $rootScope.$on('userUnauthenticated', function() {
            $state.transitionTo('login');
        });
        app.numberRegex = /^\d+$/;
        app.setImage = function(file) {
            var defer = $q.defer();
            var reader = new FileReader();
            reader.onload = function(event) {
                    defer.resolve(event.target.result);
                }
                // when the file is read it triggers the onload event above.
            reader.readAsDataURL(file);
            return defer.promise;
        }
        app.educationLevel = {
            1: "below 10th",
            2: "10th pass",
            3: "below 12th",
            4: "12th pass",
            5: "pursuing grad",
            6: "graduate and above"
        };
        app.gender = {
            'Male': 'Male',
            'Female': 'Female',
            'Other' : 'Other'
        };
        app.interviewStatus = {
            scheduled: "scheduled",
            appeared: "appeared",
            cleared: "cleared",
            joined: "joined",
            monetized: "monetized"
        };
    }
})();
