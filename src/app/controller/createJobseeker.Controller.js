(function() {
    angular.module('kickstart').controller('createJobseekerCtrl', createJobseekerCtrl);

    function createJobseekerCtrl(appService, toastService, $scope, $state, $timeout) {
        var createJobseeker = this;
        //get list of all trades
        appService.getTrades().then(function(response) {
            createJobseeker.allDropdowns = response.data;
        });
        //get list of all leaders
        appService.getLeadersAutocomplete({}).then(function(response) {
            createJobseeker.leaders = response.data;
        });
        createJobseeker.data = {
            trades: [{ name: '', value: '' }]
        }
        createJobseeker.addTrade = function() {
            createJobseeker.data.trades.push({ name: '', value: '' });
        }
        createJobseeker.removeTrade = function(index) {
            createJobseeker.data.trades.splice(index, 1);
        }
        createJobseeker.getFile = function(files, $errors) {
            if (angular.isDefined($errors) && ($errors.filesize.length || $errors.incorrectFile.length)) {
                var imageError = '';
                if ($errors.filesize.length) {
                    imageError += 'File size exceeds 5 mb for ' + $errors.filesize.join(', ');
                }
                if ($errors.incorrectFile.length) {
                    imageError += '\n Incorrect file type for ' + $errors.incorrectFile.join(', ');
                }
                toastService.showError(imageError);
            } else {
                $scope.app.setImage(files[0]).then(function(result) {
                    createJobseeker.data.file = files[0];
                })
            }
        }
        createJobseeker.create = function(callbacks) {
            appService.createJobseeker(createJobseeker.data).then(function(result) {
                $timeout(function() {
                    $state.go('main.jobseekers');
                    toastService.show('Jobseeker created');
                }, 1000);

            }).finally(function() {
                callbacks.formSubmitted();
            })
        }
        createJobseeker.goToJobseekers = function() {
            $state.go('main.jobseekers');
        }
    }
})();
