(function() {
    angular.module('kickstart').factory('appService', appService);

    function appService(Restangular, localStorageService, userService, $rootScope, globalService) {
        var self = this;
        var token = '';
        if (globalService.getIsAuthorised()) {
            token = localStorageService.get('token');
        }
        $rootScope.$on('userAuthenticated', function() {
            token = localStorageService.get('token');
        });


        function setInterviewDateTime(date) {
            if (date) {
                date.setHours(0);
                date.setMinutes(0);
                date.setSeconds(0);
            }
            return date;
        }

        // vacancies
        self.getVacancies = function(query) {
            var queryParams = query;
            queryParams.access_token = token;
            queryParams.showAll = true;
            return Restangular.all('vacancies').customGET('', queryParams);
        }
        
        //vacancies autocomplete
        self.getVacanciesAutocomplete = function(query) {
            var queryParams = query;
            queryParams.access_token = token;
            //queryParams.showAll = true;
            return Restangular.all('vacancies/autocomplete').customGET('', queryParams);
        }
        
        self.createVacancy = function(vacancy) {
            var data = new FormData();
            data.append('jobTitle', vacancy.jobTitle || '');
            data.append('compulsoryReqs', vacancy.compulsoryReqs || '');
            data.append('preferredReqs', vacancy.preferredReqs || '');
            data.append('company', vacancy.company || '');
            data.append('display_location', vacancy.display_location || '');
            data.append('salary_min', vacancy.salary_min || '');
            data.append('salary_max', vacancy.salary_max || '');
            data.append('food_accommodation', vacancy.food_accommodation || '');
            data.append('educationLevel', vacancy.educationLevel || '');
            data.append('guarantee_time', vacancy.guarantee_time || '');
            data.append('comments', vacancy.comments || '');
            data.append('age_min', vacancy.age_min || '');
            data.append('age_max', vacancy.age_max || '');
            data.append('location_name', vacancy.location_name || '');
            data.append('lat', vacancy.lat || '');
            data.append('long', vacancy.long || '');
            data.append('trade', vacancy.trade || '');
            data.append('pfesi', vacancy.pfesi || '');
            data.append('working_hours', vacancy.working_hours || '');
            data.append('communication', vacancy.communication || '');
            data.append('license', vacancy.license || '');
            data.append('computer', vacancy.computer || '');
            data.append('hasBike', vacancy.hasBike || '');
            data.append('hasSmartphone', vacancy.hasSmartphone || '');
            data.append('showTop', vacancy.showTop || '');
            vacancy.interviewDates.map(function(value) {
                data.append('interview_dates', (setInterviewDateTime(value.date) && value.date.getTime()) || '');
            });
            return Restangular.all("vacancies").withHttpConfig({
                transformRequest: angular.identity
            }).customPOST(data, undefined, {
                access_token: token
            }, {
                'Content-Type': undefined
            });
        }
        self.editVacancy = function(vacancy, vacancyId) {

            var data = new FormData();
            data.append('jobTitle', vacancy.jobTitle || '');
            data.append('compulsoryReqs', vacancy.compulsoryReqs || '');
            data.append('preferredReqs', vacancy.preferredReqs || '');
            data.append('company', vacancy.company || '');
            data.append('display_location', vacancy.display_location || '');
            data.append('salary_min', vacancy.salary_min || '');
            data.append('salary_max', vacancy.salary_max || '');
            data.append('food_accommodation', vacancy.food_accommodation || '');
            data.append('educationLevel', vacancy.educationLevel || '');
            data.append('guarantee_time', vacancy.guarantee_time || '');
            data.append('comments', vacancy.comments || '');
            data.append('age_min', vacancy.age_min || '');
            data.append('age_max', vacancy.age_max || '');
            data.append('location_name', vacancy.location_name || '');
            data.append('lat', vacancy.lat || '');
            data.append('long', vacancy.long || '');
            data.append('trade', vacancy.trade || '');
            data.append('pfesi', vacancy.pfesi || '');
            data.append('working_hours', vacancy.working_hours || '');
            data.append('communication', vacancy.communication || '');
            data.append('license', vacancy.license || '');
            data.append('computer', vacancy.computer || '');
            data.append('hasBike', vacancy.hasBike || '');
            data.append('hasSmartphone', vacancy.hasSmartphone || '');
            data.append('showTop', vacancy.showTop || '');
            vacancy.interviewDates.map(function(value) {
                data.append('interview_dates', (setInterviewDateTime(value.date) && value.date.getTime()) || '');
            });
            return Restangular.all("vacancies").withHttpConfig({
                transformRequest: angular.identity
            }).patch(data, {
                access_token: token,
                id: vacancyId
            }, {
                'Content-Type': undefined
            });
        }
        self.deleteVacancy = function(id) {
            return Restangular.all('').customOperation('remove', 'vacancies', {
                access_token: token,
                id: id
            }, {
                "Content-Type": "application/json;charset=utf-8"
            }, {});
        }

        //job seekers
        self.getJobseekers = function(query) {
            var queryParams = query;
            queryParams.access_token = token;
            queryParams.showLeaders = true;
            return Restangular.all('jobseekers').customGET('', queryParams);
        }
        
         //job seekers
        self.getJobseekersAutocomplete = function(query) {
            var queryParams = query;
            queryParams.access_token = token;
            //queryParams.showLeaders = true;
            return Restangular.all('jobseekers/autocomplete').customGET('', queryParams);
        }

        self.createJobseeker = function(jobseeker) {
            var data = new FormData();
            var dateOfBirth = (jobseeker.dateOfBirth && jobseeker.dateOfBirth.getTime().toFixed(0)) || '';
            data.append('name', jobseeker.name || '');
            data.append('mobile', jobseeker.mobile || '');
            data.append('lastSalary', jobseeker.lastSalary || '');
            data.append('location_name', jobseeker.location_name || '');
            data.append('avatar', jobseeker.file || '');
            data.append('lat', jobseeker.lat || '');
            data.append('long', jobseeker.long || '');
            data.append('educationLevel', jobseeker.educationLevel || '');
            data.append('gender', jobseeker.gender || '');
            data.append('dateOfBirth', dateOfBirth || '');
            data.append('communication', jobseeker.communication || '');
            data.append('hasBike', jobseeker.hasBike || '');
            data.append('license', jobseeker.license || '');
            data.append('hasSmartphone', jobseeker.hasSmartphone || '');
            data.append('computer', jobseeker.computer || '');
            data.append('comments', jobseeker.comments || '');
            data.append('leaderId', (jobseeker.leader && jobseeker.leader.id) || '');
            jobseeker.trades.map(function(value) {
                data.append(value.name, value.value || '');
            });
            return Restangular.all("jobseekers").withHttpConfig({
                transformRequest: angular.identity
            }).customPOST(data, undefined, {
                access_token: token
            }, {
                'Content-Type': undefined
            });
        }

        self.editJobseeker = function(jobseeker, jobseekerId, oldNumber) {
            var data = new FormData();
            var dateOfBirth = (jobseeker.dateOfBirth && jobseeker.dateOfBirth.getTime().toFixed(0)) || '';
            data.append('name', jobseeker.name || '');
            if (oldNumber != jobseeker.mobile)
                data.append('mobile', jobseeker.mobile || '');
            data.append('lastSalary', jobseeker.lastSalary || '');
            data.append('location_name', jobseeker.location_name || '');
            data.append('avatar', jobseeker.file || '');
            data.append('lat', jobseeker.lat || '');
            data.append('long', jobseeker.long || '');
            data.append('educationLevel', jobseeker.educationLevel || '');
            data.append('gender', jobseeker.gender || '');
            data.append('dateOfBirth', dateOfBirth || '');
            data.append('communication', jobseeker.communication || '');
            data.append('hasBike', jobseeker.hasBike || '');
            data.append('license', jobseeker.license || '');
            data.append('hasSmartphone', jobseeker.hasSmartphone || '');
            data.append('computer', jobseeker.computer || '');
            data.append('comments', jobseeker.comments || '');
            data.append('leaderId', jobseeker.leader.id || '');
            jobseeker.trades.map(function(value) {
                data.append(value.name, value.value || '');
            });
            return Restangular.all("jobseekers").withHttpConfig({
                transformRequest: angular.identity
            }).patch(data, {
                access_token: token,
                id: jobseekerId
            }, {
                'Content-Type': undefined
            });
        }
        self.deleteJobseeker = function(id) {
                return Restangular.all('').customOperation('remove', 'jobseekers', {
                    access_token: token,
                    id: id
                }, {
                    "Content-Type": "application/json;charset=utf-8"
                }, {});
            }
            
        //trades
        self.getTrades = function() {
                var queryParams = {
                    access_token: token
                }
                return Restangular.all('trades').customGET('', queryParams);
            }
            
        //leaders
        self.getLeaders = function(query) {
            var queryParams = query;
            queryParams.access_token = token;
            return Restangular.all('leaders').customGET('', queryParams);
        }
        
        //leaders autocomplete api
        self.getLeadersAutocomplete = function(query) {
            var queryParams = query;
            queryParams.access_token = token;
            return Restangular.all('leaders/autocomplete').customGET('', queryParams);
        }
        
        self.createLeader = function(leader) {
            return Restangular.all('leaders').customPOST(leader, undefined, { access_token: token }, {});
        }
        self.editLeader = function(leader, id, oldNumber) {
            if (oldNumber == leader.mobile)
                delete leader.mobile;
            return Restangular.all('leaders').patch(leader, {
                access_token: token,
                id: id
            }, {
                // "Content-Type": undefined
            });
        }
        self.deleteLeader = function(id) {
                return Restangular.all('').customOperation('remove', 'leaders', {
                    access_token: token,
                    id: id
                }, {
                    "Content-Type": "application/json;charset=utf-8"
                }, {});
            }
            //interviews
        self.getInterviews = function(query) {
            var queryParams = query;
            queryParams.access_token = token;
            return Restangular.all('interviews').customGET('', queryParams);
        }
        self.createInterview = function(interview) {
            var data = {
                vacancyId: interview.vacancy.id,
                jobseekerId: interview.jobseeker.id,
                interviewTime: interview.interviewTime
            }
            return Restangular.all('interviews').customPOST(data, undefined, { access_token: token }, {});
        }
        self.editInterview = function(interview, id) {
            var data = {
                status: interview.status,
                interviewTime: interview.interviewTime
            }
            if (interview.joiningDate)
                data.joiningDate = interview.joiningDate.getTime().toFixed(0);
            if (interview.leavingDate)
                data.leavingDate = interview.leavingDate.getTime().toFixed(0);
            return Restangular.all('interviews').patch(data, {
                access_token: token,
                id: id
            }, {});
        }
        self.deleteInterview = function(id) {
            return Restangular.all('').customOperation('remove', 'interviews', {
                access_token: token,
                id: id
            }, {
                "Content-Type": "application/json;charset=utf-8"
            }, {});
        }


        return self;
    }
})();
