(function() {
    angular.module('kickstart').service('globalService', globalService);

    function globalService($http, localStorageService,$location) {
        var self = this;
        self.isAuthorised = false;
        self.apiBaseUrl = '';
        var testUrl='http://api.kickstartjobs.in:3000';
        var prodUrl='http://api.kickstartjobs.in:3000';
        self.getIsAuthorised = function() {
            self.validateSession();
            return self.isAuthorised;
        };
        self.validateSession = function() {
            var token = localStorageService.get('token');
            if (token !== null && angular.isDefined(token)) {
                // $http.defaults.headers.common = {
                //     'access_token': token
                // };
                self.isAuthorised = true;
            } else {
                self.isAuthorised = false;
            }
        }
        self.getApiBaseUrl = function() {
            switch ($location.host()) {
                case 'localhost':
                    self.apiBaseUrl = testUrl;
                    break;
                case 'api.payable.in':
                    self.apiBaseUrl = testUrl;
                    break;
                case 'payable.in':
                    self.apiBaseUrl = prodUrl;
                    break;
                default:
                    self.apiBaseUrl = testUrl;
            }
            return self.apiBaseUrl;
        }
    }
})();
